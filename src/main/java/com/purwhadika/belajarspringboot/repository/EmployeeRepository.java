package com.purwhadika.belajarspringboot.repository;

import com.purwhadika.belajarspringboot.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
}
