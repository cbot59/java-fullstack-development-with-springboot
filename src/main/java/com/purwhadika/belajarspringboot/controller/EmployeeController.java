package com.purwhadika.belajarspringboot.controller;

import com.purwhadika.belajarspringboot.controller.request.EditEmployeeForm;
import com.purwhadika.belajarspringboot.controller.request.EmployeeForm;
import com.purwhadika.belajarspringboot.model.Employee;
import com.purwhadika.belajarspringboot.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.util.Map;

@Slf4j
@Controller
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/add-employee")
    public String addEmployee(Model model) {
        model.addAttribute("employeeForm", new EmployeeForm());
        return "add-employee";
    }

    @PostMapping("/add-employee")
    public String postEmployee(@Valid EmployeeForm employeeForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "add-employee";
        }

        log.debug("employeeForm: {}", employeeForm);
        Employee employee = employeeService.addEmployee(employeeForm);
        log.debug("Employee saved: {}", employee);

        return "redirect:/";
    }

    @GetMapping("/employee/{employeeId}")
    public String getEmployee(@PathVariable Integer employeeId, Model model,
                              @ModelAttribute("savedSuccess") Object savedSuccess) {
        EditEmployeeForm employeeForm = employeeService.getEmployee(employeeId);
        log.debug("get employeeForm: {}", employeeForm);

        if (null == employeeForm.getId()) {
            return "redirect:/add-employee";
        }

        model.addAttribute("editEmployeeForm", employeeForm);
        model.addAttribute(savedSuccess);

        return "employee";
    }

    @PostMapping("/employee/{employeeId}")
    public String editEmployee(@PathVariable Integer employeeId, @Valid EditEmployeeForm editEmployeeForm,
                                     RedirectAttributes redirectAttributes) {
        log.debug("updated employee: {}", editEmployeeForm);
        Employee employee = employeeService.editEmployee(editEmployeeForm);

        if (null != employee.getId()) {
            redirectAttributes.addFlashAttribute("savedSuccess", true);
        }

        return "redirect:/employee/" + employeeId;
    }

    @PostMapping("/delete-employee/{employeeId}")
    public String deleteEmployee(@PathVariable Integer employeeId) {
        employeeService.deleteEmployee(employeeId);

        return "redirect:/";
    }
}
