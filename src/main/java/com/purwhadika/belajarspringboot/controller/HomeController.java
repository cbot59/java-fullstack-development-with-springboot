package com.purwhadika.belajarspringboot.controller;

import com.purwhadika.belajarspringboot.controller.response.EmployeeResponse;
import com.purwhadika.belajarspringboot.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Locale;

@Slf4j
@Controller
public class HomeController {

    private final EmployeeService employeeService;

    public HomeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/")
    public String getHome(Model model, Locale locale) {
        log.debug("locale: {}", locale.getDisplayCountry());

        List<EmployeeResponse> employees = employeeService.getEmployees();
        model.addAttribute("employees", employees);

        return "home";
    }
}
