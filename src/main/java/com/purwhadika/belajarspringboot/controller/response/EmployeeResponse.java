package com.purwhadika.belajarspringboot.controller.response;

import com.purwhadika.belajarspringboot.model.Employee;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EmployeeResponse {
    private Integer id;
    private String name;
    private Integer age;
    private Integer salary;

    public EmployeeResponse(Employee employee) {
        id = employee.getId();
        name = employee.getName();
        age = employee.getAge();
        salary = employee.getSalary().intValue();
    }
}
