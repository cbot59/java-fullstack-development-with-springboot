package com.purwhadika.belajarspringboot.controller.request;

import com.purwhadika.belajarspringboot.model.Employee;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class EditEmployeeForm {
    private Integer id;

    @NotBlank
    private String name;

    @NotNull
    @Min(value = 17, message = "Usia minimum adalah ${min}")
    @Max(value = 60, message = "Usia maksimum adalah ${max}")
    private Integer age;

    @NotNull
    @Min(value = 2900000, message = "Gaji minimum adalah ${min}")
    @Max(value = 15000000, message = "Gaji maksimum adalah ${max}")
    private BigDecimal salary;

    private LocalDateTime registeredAt;

    public EditEmployeeForm(Employee employee) {
        id = employee.getId();
        name = employee.getName();
        age = employee.getAge();
        salary = employee.getSalary();
        registeredAt = employee.getRegisteredAt();
    }
}
