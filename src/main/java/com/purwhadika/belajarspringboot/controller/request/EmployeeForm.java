package com.purwhadika.belajarspringboot.controller.request;

import com.purwhadika.belajarspringboot.model.Employee;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class EmployeeForm {
    private Integer id;

    @NotBlank
    private String name;

    @NotNull
    @Min(value = 17, message = "Usia minimum adalah ${min}")
    @Max(value = 60, message = "Usia maksimum adalah ${max}")
    private Integer age;

    @NotNull
    @Min(value = 2900000, message = "Gaji minimum adalah ${min}")
    @Max(value = 15000000, message = "Gaji maksimum adalah ${max}")
    private BigDecimal salary;

    public EmployeeForm(Employee employee) {
        id = employee.getId();
        name = employee.getName();
        age = employee.getAge();
        salary = employee.getSalary();
    }
}
