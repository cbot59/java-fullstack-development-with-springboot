package com.purwhadika.belajarspringboot.service;

import com.purwhadika.belajarspringboot.controller.request.EditEmployeeForm;
import com.purwhadika.belajarspringboot.controller.request.EmployeeForm;
import com.purwhadika.belajarspringboot.controller.response.EmployeeResponse;
import com.purwhadika.belajarspringboot.model.Employee;
import com.purwhadika.belajarspringboot.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee addEmployee(EmployeeForm employeeForm) {
        Employee newEmployee = new Employee();
        newEmployee.setName(employeeForm.getName());
        newEmployee.setAge(employeeForm.getAge());
        newEmployee.setSalary(employeeForm.getSalary());
        newEmployee.setRegisteredAt(LocalDateTime.now());

        return employeeRepository.save(newEmployee);
    }

    public List<EmployeeResponse> getEmployees() {
        List<Employee> employees = (List<Employee>) employeeRepository.findAll();

        return employees.stream()
            .map(EmployeeResponse::new)
            .collect(Collectors.toList());
    }

    public EditEmployeeForm getEmployee(Integer employeeId) {
        return employeeRepository.findById(employeeId)
            .map(EditEmployeeForm::new)
            .orElse(new EditEmployeeForm());
    }

    public Employee editEmployee(EditEmployeeForm editEmployeeForm) {
        return employeeRepository.findById(editEmployeeForm.getId())
            .map(employee -> employeeRepository.save(employee.updateFrom(editEmployeeForm)))
            .orElse(new Employee());
    }

    public void deleteEmployee(Integer employeeId) {
        employeeRepository.deleteById(employeeId);
    }
}
