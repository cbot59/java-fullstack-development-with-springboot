package com.purwhadika.belajarspringboot.model;

import com.purwhadika.belajarspringboot.controller.request.EditEmployeeForm;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Integer age;
    private BigDecimal salary;
    private LocalDateTime registeredAt;

    public Employee updateFrom(EditEmployeeForm editEmployeeForm) {
        name = editEmployeeForm.getName();
        age = editEmployeeForm.getAge();
        salary = editEmployeeForm.getSalary();

        return this;
    }
}
